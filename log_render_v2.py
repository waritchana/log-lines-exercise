import re

def readFile(file_name):
    f = open(file_name, 'r')
    #yyyy-MM-dd HH:mm:ss,SSS - <LOG LEVEL> - <LOG TEXT>
    #group 1 - Log level --> DEBUG or ERROR or FATAL or INFO or WARNING
    #group 2 - Log text
    pattern = r'(INFO|ERROR|WARNING|FATAL|DEBUG)\s-\s(.+)'
    log_groups = re.findall(pattern, f.read())
    log_groups.sort()
    return log_groups


def resultPrinter(log_groups):
    debugs, errors, fatals, infos, warnings = {}, {}, {}, {}, {}
    debug_counter, error_counter, fatal_counter, info_counter, warning_counter = 0, 0, 0, 0, 0
    for log_group in log_groups:
        if log_group[0] == 'DEBUG':
            debugs = dictLookingUp(log_group[1], debugs)
            debug_counter += 1
        elif log_group[0] == 'ERROR':
            errors = dictLookingUp(log_group[1], errors)
            error_counter += 1
        elif log_group[0] == 'FATAL':
            fatals = dictLookingUp(log_group[1], fatals)
            fatal_counter += 1
        elif log_group[0] == 'INFO':
            infos = dictLookingUp(log_group[1], infos)
            info_counter += 1
        elif log_group[0] == 'WARNING':
            warnings = dictLookingUp(log_group[1], warnings)
            warning_counter += 1

    errors_summary = [debugs, errors, fatals, infos, warnings]
    errors_labels = ['DEBUG', 'ERROR', 'FATAL', 'INFO', 'WARNING']
    errors_counters = [
        debug_counter, error_counter, fatal_counter,
        info_counter, warning_counter
    ]

    #print results to console
    for i in range(len(errors_labels)):
        if len(errors_summary[i]) == 0:
            continue
        else:
            print errors_labels[i], 'lines'
            print errors_counters[i], 'Total lines'
            for key in errors_summary[i].keys():
                print key, '-', errors_summary[i][key], 'instances'
        print '\n'


def dictLookingUp(log_text, log_group):
    if log_text not in log_group:
        log_group[log_text] = 1
    else:
        log_group[log_text] += 1
    return log_group


log_groups = readFile('input_file.txt')
resultPrinter(log_groups)
