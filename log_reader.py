def readFile(filename):
    input_file = open(filename, 'r')
    lines = []
    for line in input_file:
        lines.append(line)
    return lines


def extractLog(lines):
    type_counters = {}
    infos = {}
    errors = {}
    warnings = {}
    fatals = {}
    debugs = {}

    for line in lines:
        log_data = line.split('-')
        if log_data[3] not in type_counters:
            type_counters[log_data[3]] = 1
        else:
            if type_counters[log_data[3]] < 5:
                type_counters[log_data[3]] += 1
            else:
                continue

        if log_data[3] == ' INFO ' and len(infos) < 5:
            if log_data[4] not in infos:
                infos[log_data[4]] = 1
            else:
                infos[log_data[4]] += 1
        elif log_data[3] == ' ERROR ' and len(errors) < 5:
            if log_data[4] not in errors:
                errors[log_data[4]] = 1
            else:
                errors[log_data[4]] += 1
        elif log_data[3] == ' WARNING ' and len(warnings) < 5:
            if log_data[4] not in warnings:
                warnings[log_data[4]] = 1
            else:
                warnings[log_data[4]] += 1
        elif log_data[3] == ' FATAL ' and len(fatals) < 5:
            if log_data[4] not in fatals:
                fatals[log_data[4]] = 1
            else:
                fatals[log_data[4]] += 1
        elif log_data[3] == ' DEBUG ' and len(debugs) < 5:
            if log_data[4] not in debugs:
                debugs[log_data[4]] = 1
            else:
                debugs[log_data[4]] += 1
        else:
            continue

    return type_counters, infos, errors, warnings, fatals, debugs

def outputPrinter(type_counters, infos, errors, warnings, fatals, debugs):
    for key in type_counters:
        print key[1:-1] + ' lines'
        count = 1
        if type_counters[key] == 1:
            print str(type_counters[key]) + ' Total Line'
        elif type_counters[key] > 1:
            print str(type_counters[key]) + ' Total Lines'
        if key[1:-1] == 'INFO':
            print infos
            for each in infos:
                print str(count) + ') ' + each + ' - ' + str(infos[each]) + ' instances'
                count += 1
        elif key[1:-1] == 'ERROR':
            for each in errors:
                print str(count) + ') ' + each + ' - ' + str(errors[each]) + ' instances'
                count += 1
        elif key[1:-1] == 'WARNING':
            for each in warnings:
                print str(count) + ') ' + each + ' - ' + str(warnings[each]) + ' instances'
                count += 1
        elif key[1:-1] == 'FATAL':
            for each in fatals:
                print str(count) + ') ' + each + ' - ' + str(fatals[each]) + ' instances'
                count += 1
        elif key[1:-1] == 'DEBUGS':
            for each in debugs:
                print str(count) + ') ' + each + ' - ' + debugs[each] + ' instances'
                count += 1
        print '-------------------'

lines = readFile('input_file.txt')
type_counters, infos, errors, warnings, fatals, debugs = extractLog(lines)
outputPrinter(type_counters, infos, errors, warnings, fatals, debugs)
